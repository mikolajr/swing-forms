package org.rydzewski.swing.binding;

import junit.framework.TestCase;
import org.mockito.Mockito;
import org.rydzewski.swing.binding.errors.BindingError;
import org.rydzewski.swing.binding.errors.LengthBindingError;
import org.rydzewski.swing.binding.errors.NumericBindingError;
import org.rydzewski.swing.binding.errors.RegexpBindingError;
import org.rydzewski.swing.binding.errors.RequiredBindingError;

import javax.swing.*;


public class BindingTest extends TestCase {

    public void testVerifyRequired() {
        Mapping mapping = Mockito.mock(Mapping.class);
        Mockito.when(mapping.required()).thenReturn(true);
        Mockito.when(mapping.regexp()).thenReturn("");
        Mockito.when(mapping.scale()).thenReturn(-1);
        Mockito.when(mapping.length()).thenReturn(-1);

        BindingError verify = createBinding("", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RequiredBindingError);
    }

    public void testVerifyLength() {
        Mapping mapping = Mockito.mock(Mapping.class);
        Mockito.when(mapping.required()).thenReturn(true);
        Mockito.when(mapping.length()).thenReturn(3);
        Mockito.when(mapping.regexp()).thenReturn("");
        Mockito.when(mapping.scale()).thenReturn(-1);

        BindingError verify = createBinding("", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RequiredBindingError);

        verify = createBinding("a", mapping).verify();
        assertNull(verify);

        verify = createBinding("fooshmoo", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof LengthBindingError);
    }

    public void testVerifyPattern() {
        Mapping mapping = Mockito.mock(Mapping.class);
        Mockito.when(mapping.required()).thenReturn(true);
        Mockito.when(mapping.length()).thenReturn(-1);
        Mockito.when(mapping.regexp()).thenReturn("[a-z][0-9]{3}");
        Mockito.when(mapping.scale()).thenReturn(-1);

        BindingError verify = createBinding("", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RequiredBindingError);

        verify = createBinding("a", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RegexpBindingError);

        verify = createBinding("12345", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RegexpBindingError);

        verify = createBinding("b345", mapping).verify();
        assertNull(verify);

        verify = createBinding("a123v", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RegexpBindingError);
    }

    public void testVerifyNumericPattern() {
        Mapping mapping = Mockito.mock(Mapping.class);
        Mockito.when(mapping.required()).thenReturn(true);
        Mockito.when(mapping.length()).thenReturn(-1);
        Mockito.when(mapping.regexp()).thenReturn(IntegerBinding.NUMERIC_PATTERN);
        Mockito.when(mapping.scale()).thenReturn(-1);

        BindingError verify = createBinding("", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RequiredBindingError);

        verify = createBinding("a", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RegexpBindingError);

        verify = createBinding("12345", mapping).verify();
        assertNull(verify);

        verify = createBinding("-1", mapping).verify();
        assertNull(verify);
    }

    public void testVerifyNotRequiredPattern() {
        Mapping mapping = Mockito.mock(Mapping.class);
        Mockito.when(mapping.required()).thenReturn(false);
        Mockito.when(mapping.length()).thenReturn(-1);
        Mockito.when(mapping.regexp()).thenReturn("[0-9]{2}:[0-9]{2}");
        Mockito.when(mapping.scale()).thenReturn(-1);

        BindingError verify = createBinding("", mapping).verify();
        assertNull(verify);

        verify = createBinding(null, mapping).verify();
        assertNull(verify);

        verify = createBinding("a", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RegexpBindingError);

        verify = createBinding("12345", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RegexpBindingError);

        verify = createBinding("12:24", mapping).verify();
        assertNull(verify);
    }

    public void testVerifyScale() {
        Mapping mapping = Mockito.mock(Mapping.class);
        Mockito.when(mapping.required()).thenReturn(true);
        Mockito.when(mapping.length()).thenReturn(-1);
        Mockito.when(mapping.scale()).thenReturn(2);
        Mockito.when(mapping.regexp()).thenReturn("");

        BindingError verify = createBinding("", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof RequiredBindingError);

        verify = createBinding("a", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof NumericBindingError);

        verify = createBinding("12345", mapping).verify();
        assertNull(verify);

        verify = createBinding("123.1", mapping).verify();
        assertNull(verify);

        verify = createBinding("123.12", mapping).verify();
        assertNull(verify);

        verify = createBinding("123.100", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof NumericBindingError);

        verify = createBinding("12345.3456", mapping).verify();
        assertNotNull(verify);
        assertTrue(verify instanceof NumericBindingError);
    }

    private Binding createBinding(String value, Mapping mapping) {
        JTextField textField = new JTextField(value);
        return new TextBinding(null, mapping, textField, null, null);
    }

}
