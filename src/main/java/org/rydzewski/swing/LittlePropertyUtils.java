package org.rydzewski.swing;

import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.Field;

/**
 * Utilities and additions for commons-beanutils
 * @author Mikolaj Rydzewski
 */
public class LittlePropertyUtils {
    
    /**
     * Returns bean owning property name.
     * Having propertyName of 'items.item.tax.value' will return bean referenced by 'items.item.tax', assuming
     * that sourceBean has property called 'items' and so on... 
     * @param sourceBean
     * @param propertyName
     * @return
     */
    public static Object getTargetBean(Object sourceBean, String propertyName) {
        while (propertyName.indexOf('.') != -1) {
            String bean = propertyName.substring(0, propertyName.indexOf('.'));
            try {
                sourceBean = PropertyUtils.getProperty(sourceBean, bean);
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
            propertyName = propertyName.substring(propertyName.indexOf('.')+1);
        }
        return sourceBean;
    }
    
    /**
     * Returns last property from long property name.
     * Having propertyName of 'items.item.tax.value' will return 'value'.
     * @param propertyName
     * @return
     */
    public static String getTargetProperty(String propertyName) {
        return propertyName.indexOf('.') == -1 ? propertyName : propertyName.substring(propertyName.lastIndexOf('.')+1); 
    }

    /**
     * Returns Field object describing property of its owning bean. Uses {@link #getTargetBean(Object, String)}
     * and {@link #getTargetProperty(String)} to resolve nested properties.
     * @param bean
     * @param propertyName
     * @return
     * @throws SecurityException
     * @throws NoSuchFieldException
     */
    public static Field getTargetDeclaredField(Object bean, String propertyName) throws SecurityException, NoSuchFieldException {
        Object targetBean = getTargetBean(bean, propertyName);
        String targetProperty = getTargetProperty(propertyName);
        return targetBean.getClass().getDeclaredField(targetProperty);
    }
}
