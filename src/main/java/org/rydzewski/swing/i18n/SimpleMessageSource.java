package org.rydzewski.swing.i18n;

public interface SimpleMessageSource {
    boolean containsKey(String key);
    String getString(String key);
}
