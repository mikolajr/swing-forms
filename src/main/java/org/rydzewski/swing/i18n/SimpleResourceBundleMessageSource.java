package org.rydzewski.swing.i18n;

import java.util.ResourceBundle;

public class SimpleResourceBundleMessageSource implements SimpleMessageSource {

    private ResourceBundle resourceBundle;

    public SimpleResourceBundleMessageSource(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    @Override
    public boolean containsKey(String key) {
        return resourceBundle.containsKey(key);
    }

    @Override
    public String getString(String key) {
        return resourceBundle.getString(key);
    }
}
