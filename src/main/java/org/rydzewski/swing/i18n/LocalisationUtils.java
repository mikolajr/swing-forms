package org.rydzewski.swing.i18n;

import org.apache.commons.lang.StringUtils;
import org.rydzewski.swing.LittlePropertyUtils;
import org.rydzewski.swing.binding.Mapping;

import java.util.ResourceBundle;

public class LocalisationUtils {

    private SimpleMessageSource messageSource;

    public LocalisationUtils(SimpleMessageSource simpleMessageSource) {
        this.messageSource = simpleMessageSource;
    }

    public LocalisationUtils(ResourceBundle resourceBundle) {
        this.messageSource = new SimpleResourceBundleMessageSource(resourceBundle);
    }

    private String getLastPropertyFromChainedName(String property) {
        int lastDot = property.lastIndexOf('.');
        return lastDot == -1 ? property : property.substring(lastDot+1);
    }

    private String getMessageString(Object bean, String property, String suffix) {
        String className = LittlePropertyUtils.getTargetBean(bean, property).getClass().getName();
        String key = String.format("%s.%s%s", className, getLastPropertyFromChainedName(property), StringUtils.defaultString(suffix));
        if (messageSource.containsKey(key)) {
            return messageSource.getString(key);
        } else if (suffix == null) {
            System.err.println("No message for key: " + key);
        }
        return null;
    }

    public String getTooltipFromMapping(Mapping mapping, Object bean, String property) {
        String tooltip = getMessageString(bean, property, "-tooltip");
        return tooltip != null ? tooltip : StringUtils.stripToNull(mapping.tooltip());
    }

    public String getLabelFromMapping(Mapping mapping, Object bean, String property) {
        if (bean != null) {
            String message = getMessageString(bean, property, null);
            return message != null ? message : mapping.label();
        } else {
            return mapping.label();
        }
    }
}
