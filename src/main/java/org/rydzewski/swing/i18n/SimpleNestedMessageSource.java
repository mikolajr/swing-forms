package org.rydzewski.swing.i18n;

public class SimpleNestedMessageSource implements SimpleMessageSource {

    private SimpleMessageSource[] messageSources;

    public SimpleNestedMessageSource(SimpleMessageSource... messageSources) {
        this.messageSources = messageSources;
    }

    @Override
    public boolean containsKey(String key) {
        for (SimpleMessageSource messageSource : messageSources) {
            if (messageSource.containsKey(key)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String getString(String key) {
        for (SimpleMessageSource messageSource : messageSources) {
            if (messageSource.containsKey(key)) {
                return messageSource.getString(key);
            }
        }
        return null;
    }
}
