/**
 * 
 */
package org.rydzewski.swing.components;

import org.rydzewski.swing.binding.LabelValue;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class DefaultComboDataProvider implements ComboDataProvider {

    private List<LabelValue> list;
    private boolean allowEmptyValue = true;

    @Override
    public List<LabelValue> getValues(JComboBox comboBox) {
        return list;
    }

    public void setList(List<LabelValue> list) {
        this.list = list;
    }
    
    public void setProperties(Properties properties) {
        list = new ArrayList<LabelValue>();
        for(Object key :  properties.keySet()) {
            String value = properties.getProperty((String) key);
            String label = key.equals(value) ? value : String.format("%s - %s", key, value); 
            list.add(new LabelValue(label, (String) key));
        }
        Collections.sort(list);
    }
    
    @SuppressWarnings("unchecked")
    public void setStrings(List strings) {
        list = new ArrayList<LabelValue>();
        for(Object s :  strings) {
            list.add(new LabelValue(s.toString(), s.toString()));
        }
        Collections.sort(list);
    }

    public LabelValue getLabelValueForKey(String key) {
        for (LabelValue labelValue : list) {
            if (key.equals(labelValue.getValue())) {
                return labelValue;
            }
        }
        return null;
    }

    @Override
    public boolean isAllowEmptyValue() {
        return allowEmptyValue;
    }

    public void setAllowEmptyValue(boolean allowEmptyValue) {
        this.allowEmptyValue = allowEmptyValue;
    }
}
