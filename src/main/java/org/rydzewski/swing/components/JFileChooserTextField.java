/**
 * 
 */
package org.rydzewski.swing.components;

import org.apache.commons.lang.StringUtils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class JFileChooserTextField extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = -5006391676159987950L;

	public enum Mode { OPEN, SAVE };
	
	private JTextField filename;
	private JButton browseButton;
	private Mode mode;
	private File directory;
	private String filemask;
	private int selectionMode;

	public JFileChooserTextField(Mode mode, String filename, File directory, String filemask, int selectionMode) {
		super();
		this.mode = mode;
		this.directory = directory;
		this.filemask = filemask;
		this.selectionMode = selectionMode;
		
		this.filename = createTextField();
		this.filename.setText(filename);
		
		browseButton = createBrowseButton();
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(this.filename);
		add(browseButton);
	}

	protected JButton createBrowseButton() {
		JButton button = new JButton("...");
		button.addActionListener(this);
		return button;
	}

	protected JTextField createTextField() {
		return new JTextField();
	}

	public JTextField getFilename() {
		return filename;
	}

	public JButton getBrowseButton() {
		return browseButton;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser chooser = createFileChooser();
		int result = mode == Mode.OPEN ? chooser.showOpenDialog(this) : chooser.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			filename.setText(chooser.getSelectedFile().getAbsolutePath());
		}
	}

	protected JFileChooser createFileChooser() {
		JFileChooser fileChooser = new JFileChooser(directory);
		fileChooser.setFileFilter(createFileFilter(filemask));
		fileChooser.setFileSelectionMode(selectionMode);
		
		if (mode == Mode.OPEN && StringUtils.isNotEmpty(filename.getText())) {
			File directory = new File(filename.getText()).getParentFile();
			fileChooser.setCurrentDirectory(directory);
		}
		
		if (mode == Mode.SAVE && StringUtils.isNotEmpty(filename.getText())) {
			File directory = new File(filename.getText());
			fileChooser.setCurrentDirectory(directory);
		}
		
		return fileChooser;
	}

	protected FileFilter createFileFilter(String filemask) {
		if (filemask == null)
			return null;
		
		int pipe = filemask.indexOf('|');
		final String desc = pipe > 0 ? filemask.substring(0, pipe) : filemask;
		final String glob = pipe > 0 ? filemask.substring(pipe + 1) : filemask;
		
		return new FileFilter() {
			@Override
			public String getDescription() {
				return desc;
			}
			
			@Override
			public boolean accept(File f) {
				return f.getName().matches(glob);
			}
		};
	}

	public String getText() {
		return filename.getText();
	}

	public void setText(String t) {
		filename.setText(t);
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

	public File getDirectory() {
		return directory;
	}

	public void setDirectory(File directory) {
		this.directory = directory;
	}

	public String getFilemask() {
		return filemask;
	}

	public void setFilemask(String filemask) {
		this.filemask = filemask;
	}

	public int getSelectionMode() {
		return selectionMode;
	}

	public void setSelectionMode(int selectionMode) {
		this.selectionMode = selectionMode;
	}

	public void setFilename(JTextField filename) {
		this.filename = filename;
	}

	public void setBrowseButton(JButton browseButton) {
		this.browseButton = browseButton;
	}

	@Override
	public void setEnabled(boolean enabled) {
		browseButton.setEnabled(enabled);
		filename.setEditable(enabled);
	}

	public boolean isEmpty() {
		return StringUtils.isEmpty(filename.getText());
	}

	public boolean isNotEmpty() {
		return StringUtils.isNotEmpty(filename.getText());
	}

	public void setToolTipText(String text) {
		filename.setToolTipText(text);
	}
	
}
