/**
 * 
 */
package org.rydzewski.swing.components;

import com.toedter.calendar.JDateChooser;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Date;
import java.util.Locale;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public class JSimpleDateChooser extends JDateChooser implements DateComponent {

	private static final long serialVersionUID = -8837976487304638634L;

    private DateTimeFormatter formatter;

    public JSimpleDateChooser(String dateFormat) {
		super();
		setDateFormatString(dateFormat);
        String country = Locale.getDefault().getCountry();
        setLocale(new Locale(country));
        formatter = DateTimeFormatter.ofPattern(dateFormat);
	}

    @Override
    public void setFormatter(DateTimeFormatter dateTimeFormatter) {
        this.formatter = dateTimeFormatter;
    }

    @Override
    public String getStringValue() {
        Date date = getDate();
        return date != null ? formatter.format(LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()).toLocalDate()) : null;
    }

    @Override
    public void setValue(Object value) {
        try {
            if (value instanceof Date) {
                setDate((Date) value);
            } else {
                TemporalAccessor parsed = formatter.parse((String) value);
                setDate(Date.from(Instant.from(parsed)));
            }
        }
        catch (Exception e) {
            // NOPMD
        }
    }

    public void addDateChangedListener(final DateChangedListener listener) {
	    getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent e) {
                if ("date".equals(e.getPropertyName())) {
                    listener.onDateChanged((Date) e.getNewValue());
                }
            }
        });
	}

}
