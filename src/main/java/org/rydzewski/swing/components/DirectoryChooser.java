package org.rydzewski.swing.components;

import org.apache.commons.lang.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

/**
 * @author wzupa
 */
public class DirectoryChooser extends JPanel implements ActionListener {

	private static final long serialVersionUID = 8295596412163073834L;

	private final String buttonText;
	private final String buttonIcon;
	private final boolean fieldReadOnly;
	private JFileChooser fileChooser;
	private final String rootDirectory;

	private JTextField directoryPath;
	private JButton browseButton;

	private int currentFileSelectionMode;
	private FileFilter[] currentFileFilters;

	public DirectoryChooser(String buttonText, String buttonIcon, boolean fieldReadOnly, JFileChooser jFileChooser, String rootDirectory) {
		this.buttonText = buttonText;
		this.buttonIcon = buttonIcon;
		this.fieldReadOnly = fieldReadOnly;
		this.fileChooser = jFileChooser;
		this.rootDirectory = rootDirectory;

		this.directoryPath = createDirectoryPathField();
		this.browseButton = createBrowseButton();
		JPanel separatorPanel = new JPanel();
		separatorPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(directoryPath);
		add(separatorPanel);
		add(browseButton);
	}

	private JTextField createDirectoryPathField() {
		JTextField dirPath = new JTextField();
		if (fieldReadOnly) {
			dirPath.setEditable(false);
		}
		return dirPath;
	}

	private JButton createBrowseButton() {
		JButton browseButton = new JButton(buttonText);
		if (StringUtils.isNotBlank(buttonIcon)) {
			try {
				URL imageUrl = this.getClass().getResource(buttonIcon);
				browseButton.setIcon(new ImageIcon(imageUrl));
			} catch (Exception e) {
				// ignored
			}
		}
		browseButton.addActionListener(this);
		return browseButton;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fChooser = createFileChooser();
		int result = fChooser.showOpenDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
			directoryPath.setText(fChooser.getSelectedFile().getAbsolutePath());
		}

		fChooser.setFileSelectionMode(currentFileSelectionMode);
		for (FileFilter filter : currentFileFilters) {
			fChooser.addChoosableFileFilter(filter);
		}
	}

	private JFileChooser createFileChooser() {
		JFileChooser fc = fileChooser != null ? fileChooser : new JFileChooser(rootDirectory);
		currentFileSelectionMode = fc.getFileSelectionMode();
		currentFileFilters = fc.getChoosableFileFilters();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.resetChoosableFileFilters();
		return fc;
	}

	public String getValue() {
		return directoryPath.getText();
	}

	public void setValue(String path) {
		directoryPath.setText(path);
	}
}
