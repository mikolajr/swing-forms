/**
 * 
 */
package org.rydzewski.swing.components;

import org.apache.commons.lang.StringUtils;
import org.rydzewski.swing.binding.LabelValue;

import javax.swing.*;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class LengthVerifier extends InputVerifier {

    private final int size;

    public LengthVerifier(int maxSize) {
        this.size = maxSize;
    }

    @Override
    public boolean verify(JComponent input) {
        if (input instanceof JComboBox) {
            Object item = ((JComboBox)input).getSelectedItem();
            if (item instanceof String) {
                return StringUtils.defaultString((String) item).length() <= size;
            }
            if (item instanceof LabelValue) {
                LabelValue lv = (LabelValue) item;
                return StringUtils.defaultString(lv.getValue()).length() <= size;
            }
        } else if (input instanceof JTextComponent) {
            String s = ((JTextComponent)input).getText();
            return StringUtils.defaultString(s).length() <= size;
        }
        
        return true;
    }

}
