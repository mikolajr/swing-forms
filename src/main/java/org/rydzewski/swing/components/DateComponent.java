package org.rydzewski.swing.components;

import java.time.format.DateTimeFormatter;

/**
 * Interface for date picker components.
 *
 * @see org.rydzewski.swing.binding.FieldBinder
 * @see org.rydzewski.swing.binding.DateBinding
 * @author wzupa
 */
public interface DateComponent {

    /**
     * Get current date value as string.
     *
     * @return
     */
    String getStringValue();

    /**
     * Sets current date value.
     *
     * @param object
     */
    void setValue(Object object);

    /**
     * Sets components date format.
     *
     * @param dateTimeFormatter
     */
    void setFormatter(DateTimeFormatter dateTimeFormatter);
}
