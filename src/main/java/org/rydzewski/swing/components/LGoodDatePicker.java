package org.rydzewski.swing.components;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * @author wzupa
 */
public class LGoodDatePicker extends DatePicker implements DateComponent {

    private DateTimeFormatter formatter;
    private DatePickerSettings settings = new DatePickerSettings();

    public LGoodDatePicker() {
        settings.setLocale(Locale.getDefault());
        setSettings(settings);
    }

    @Override
    public void setFormatter(DateTimeFormatter dateTimeFormatter) {
        this.formatter = dateTimeFormatter;
        settings.setFormatForDatesCommonEra(dateTimeFormatter);
    }

    @Override
    public String getStringValue() {
        LocalDate date = getDate();
        return date != null ? date.format(formatter) : null;
    }

    @Override
    public void setValue(Object value) {
        try {
            if (value instanceof Date) {
                setDate(LocalDate.from(((Date) value).toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
            } else if (value instanceof LocalDate) {
                setDate((LocalDate) value);
            } else {
                setDate(LocalDate.from(formatter.parse((String) value)));
            }
        } catch (Exception e) {
            // NOPMD
        }
    }
}
