/**
 * 
 */
package org.rydzewski.swing.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class JColorChooserTextField extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = -5006391676159987950L;

	private JTextField sample;
	private JButton browseButton;
	private Color color;
	private String title;

	public JColorChooserTextField() {
		this.sample = createTextField();
		
		browseButton = createBrowseButton();
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(this.sample);
		add(browseButton);
	}

	protected JButton createBrowseButton() {
		JButton button = new JButton("...");
		button.addActionListener(this);
		return button;
	}

	protected JTextField createTextField() {
		JTextField jTextField = new JTextField();
		jTextField.setHorizontalAlignment(JTextField.CENTER);
        return jTextField;
	}

	public JTextField getSample() {
		return sample;
	}

	public JButton getBrowseButton() {
		return browseButton;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	    Color newColor = JColorChooser.showDialog(this, title, color);
	    if (newColor != null) {
	        setColor(newColor);
	    }
	}

	public String getText() {
		return sample.getText();
	}

	public void setText(String t) {
		sample.setText(t);
	}

	public void setSample(JTextField filename) {
		this.sample = filename;
	}

	public void setBrowseButton(JButton browseButton) {
		this.browseButton = browseButton;
	}

	@Override
	public void setEnabled(boolean enabled) {
		browseButton.setEnabled(enabled);
		sample.setEditable(enabled);
	}

	public void setToolTipText(String text) {
		sample.setToolTipText(text);
	}

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        sample.setBackground(color);
        this.color = color;
    }

    public void setTitle(String title) {
        this.title = title;
    }
	
}
