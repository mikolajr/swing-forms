/**
 * 
 */
package org.rydzewski.swing.components;

import java.util.Date;
import java.util.EventListener;

/**
 *
 * @author Mikolaj Rydzewski
 */
public abstract class DateChangedListener implements EventListener {
    
    public abstract void onDateChanged(Date date);

}
