package org.rydzewski.swing.components;

import org.rydzewski.swing.binding.LabelValue;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class LocalizedComboDataProvider extends DefaultComboDataProvider implements BeanNameAware, InitializingBean {

    private MessageSource messageSource;
    private String name;
    private List<String> keys;
    private boolean sortByMessageOnly;

    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void setBeanName(String name) {
        this.name = name;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        List<LabelValue> list = new ArrayList<LabelValue>();
        for (String key : keys) {
            String messageKey = String.format("%s.%s", name, key);
            String message = messageSource.getMessage(messageKey, null, Locale.getDefault());

            if (!key.equals(message)) {
                message = String.format("%s - %s", key, message);
            }

            list.add(new LabelValue(message, key));
        }
        if (sortByMessageOnly) {
            Collections.sort(list, new MessageOnlyComparator());
        } else {
            Collections.sort(list);
        }
        setList(list);
        keys = null;
    }

    public void setSortByMessageOnly(boolean sortByMessageOnly) {
        this.sortByMessageOnly = sortByMessageOnly;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    private class MessageOnlyComparator implements Comparator<LabelValue> {
        @Override
        public int compare(LabelValue o1, LabelValue o2) {
            String message1 = getMessage(o1.getValue());
            String message2 = getMessage(o2.getValue());
            return message1.compareTo(message2);
        }

        private String getMessage(String key) {
            String messageKey = String.format("%s.%s", name, key);
            return messageSource.getMessage(messageKey, null, Locale.getDefault());
        }
    }
}
