/**
 * 
 */
package org.rydzewski.swing.components;

import javax.swing.*;
import java.util.ResourceBundle;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public class TimeComponent extends JPanel {

    private static final long serialVersionUID = -3768516946871485717L;
    
    private JSpinner hours;
    private JSpinner minutes;

    public TimeComponent(ResourceBundle resourceBundle) {
        hours = new JSpinner(new SpinnerNumberModel(0, 0, 23, 1));
        hours.setToolTipText(resourceBundle.getString("time.tooltip.hour"));
        minutes = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        minutes.setToolTipText(resourceBundle.getString("time.tooltip.minute"));

        add(hours);
        add(minutes);
    }

    public void setValue(String time) {
        String[] split = time.split(":");
        if (split.length == 2) {
            hours.setValue(Integer.valueOf(split[0]));
            minutes.setValue(Integer.valueOf(split[1]));
        }
    }

    public void setValue(int hour, int minute) {
        hours.setValue(hour);
        minutes.setValue(minute);
    }

    public String getValue() {
        return String.format("%02d:%02d", hours.getValue(), minutes.getValue());
    }
    
}
