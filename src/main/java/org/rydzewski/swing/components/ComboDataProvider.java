/**
 * 
 */
package org.rydzewski.swing.components;

import org.rydzewski.swing.binding.LabelValue;

import javax.swing.*;
import java.util.List;

/**
 * Data provider for {@link org.rydzewski.swing.binding.ComboBinding}
 * @author Mikolaj Rydzewski
 */
public interface ComboDataProvider {
    
    /**
     * Returns list of values for combo box's list
     * @param comboBox
     * @return
     */
    List<LabelValue> getValues(JComboBox comboBox);

    /**
     * Whether list bound to combo will contain empty row
     * @return
     */
    boolean isAllowEmptyValue();

}
