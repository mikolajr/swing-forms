/**
 * 
 */
package org.rydzewski.swing;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class SimplePanelBuilder extends PanelBuilder {
	
	/**
	 * row specification for new rows with controls
	 * TODO adding top: causes look not to be nice
	 */
	private String defaultRow = "p";
	
	/**
	 * row specification for new separator rows 
	 */
	private String defaultSpacer = "3dlu";

	public SimplePanelBuilder(FormLayout layout, JPanel panel) {
		super(layout, panel);
		setDefaultDialogBorder();
		appendRow(defaultRow);
		
	}

	public SimplePanelBuilder(FormLayout layout) {
		super(layout);
		setDefaultDialogBorder();
		appendRow(defaultRow);
	}
	
	/**
	 * Adds component using default constraints. Move cursor two columns to the right.
	 * @param component
	 * @return added component
	 */
	public Component addAndMove(Component component) {
		return addAndMove(component, 1);
	}
	
	/**
	 * Adds component using specified column span. Moves cursor colSpan+1 columns to the right
	 * @param component
	 * @param colSpan
	 * @return added component
	 */
	public Component addAndMove(Component component, int colSpan) {
		CellConstraints cc = new CellConstraints();
		add(component, cc.rchw(getRow(), getColumn(), 1, colSpan));
		nextColumn(colSpan + 1);
		return component;
	}
	
	public JLabel addLabelAndMove(String label) {
		return (JLabel) addAndMove(new JLabel(String.format("<html><p>%s</p></html>", label)));
	}

	public void setDefaultRow(String defaultRow) {
		this.defaultRow = defaultRow;
	}

	public void setDefaultSpacer(String defaultSpacer) {
		this.defaultSpacer = defaultSpacer;
	}

	/**
	 * Adds new form row: appends separator row and control row. 
	 */
	public void nextFormLine() {
	    appendSpacerRow();
		appendRow(defaultRow);
		nextLine();
	}
	
	public void appendSpacerRow() {
	    appendRow(defaultSpacer);
	    nextLine();
	}
	
	/**
	 * Adds specified row and moves cursor.
	 * @param rowSpec
	 */
	public void addSimpleRow(String rowSpec) {
	    appendRow(rowSpec);
	    nextLine();
	}
	
	/**
	 * Adds new control row.
	 */
	public void nextSimpleLine() {
		appendRow(defaultRow);
		nextLine();
	}
	
    public void addButtons(JPanel buttons) {
        addButtons(buttons, "bottom,right");
    }
    
	public void addButtons(JPanel buttons, String rowSpec) {
		CellConstraints cc = new CellConstraints();
		add(buttons, cc.rchw(getRow(), 1, 1, getColumnCount(), rowSpec));
	}
}
