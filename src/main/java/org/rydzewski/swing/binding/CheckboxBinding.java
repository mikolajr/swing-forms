/**
 * 
 */
package org.rydzewski.swing.binding;

import org.apache.commons.lang.BooleanUtils;
import org.rydzewski.swing.binding.errors.BindingError;

import javax.swing.*;
import java.lang.reflect.Field;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class CheckboxBinding extends Binding {

    public CheckboxBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public Boolean getComponentValue() {
        return getComponent().isSelected();
    }

    @Override
    public void setComponentValue(Object value) {
        getComponent().setSelected(BooleanUtils.toBoolean((Boolean) value));
    }

    @Override
    public JCheckBox getComponent() {
        return (JCheckBox) super.getComponent();
    }

    @Override
    public BindingError verify() {
        return null;
    }

}
