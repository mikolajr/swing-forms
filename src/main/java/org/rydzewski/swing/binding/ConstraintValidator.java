package org.rydzewski.swing.binding;

import org.apache.commons.lang.math.NumberUtils;

/**
 * @author wzupa
 */
public class ConstraintValidator {

	private final Constraint constraint;

	private boolean valid;
	private String errorCode;

	public ConstraintValidator(Constraint constraint) {
		this.constraint = constraint;
	}

	public void validate(String value) {
		valid = false;
		errorCode = "";

		switch (constraint) {
			case GREATER_THAN_ZERO:
				double val = NumberUtils.toDouble(value);
				valid = val > 0;
				errorCode = val <= 0 ? "binding.error.constraint.GREATER_THAN_ZERO" : "";
				break;
			case GREATER_OR_EQUALS_ZERO:
				double val2 = NumberUtils.toDouble(value);
				valid = val2 >= 0;
				errorCode = val2 < 0 ? "binding.error.constraint.GREATER_OR_EQUALS_ZERO" : "";
				break;
			case LOWER_OR_EQUALS_ZERO:
				double val3 = NumberUtils.toDouble(value);
				valid = val3 <= 0;
				errorCode = val3 > 0 ? "binding.error.constraint.LOWER_OR_EQUALS_ZERO" : "";
				break;
		}
	}

	public boolean isValid() {
		return valid;
	}

	public String getErrorCode() {
		return errorCode;
	}
}
