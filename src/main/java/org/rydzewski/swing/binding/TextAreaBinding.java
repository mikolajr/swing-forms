package org.rydzewski.swing.binding;

import javax.swing.JComponent;
import javax.swing.JTextArea;
import java.lang.reflect.Field;

/**
 * @author wzupa
 */
public class TextAreaBinding extends Binding {

	public TextAreaBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
		super(field, mapping, component, bean, property);
	}

	@Override
	public JTextArea getComponent() {
		return (JTextArea) super.getComponent();
	}

	@Override
	public String getComponentValue() {
		return getComponent().getText();
	}

	@Override
	public void setComponentValue(Object value) {
		getComponent().setText((String) value);
		getComponent().select(0, 0);
	}
}
