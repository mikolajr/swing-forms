/**
 * 
 */
package org.rydzewski.swing.binding;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import javax.swing.*;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public class BigDecimalBinding extends Binding {

    public static final String NUMERIC_PATTERN = "[-]{0,1}[.0-9]*";

    public BigDecimalBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
        if (getPattern() == null) {
            setPattern(Pattern.compile(NUMERIC_PATTERN));
        }
    }

    @Override
    public BigDecimal getComponentValue() {
        try {
            return NumberUtils.createBigDecimal(StringUtils.defaultIfBlank(getComponent().getText(), "0"));
        }
        catch (Exception e) {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public void setComponentValue(Object value) {
        getComponent().setText(value != null ? value.toString() : null);
    }

    @Override
    public JTextField getComponent() {
        return (JTextField) super.getComponent();
    }

    @Override
    protected String getComponentValueForVerification() {
        return getComponent().getText();
    }
}
