package org.rydzewski.swing.binding;

/**
 * Describes visual date/time component library to use as date/time picker.
 *
 * @author wzupa
 */
public enum DateTimeComponentLibrary {

    /**
     * Default. Uses <a href="https://github.com/toedter/jcalendar">JCalendar</a>.
     * Version: 1.3.2
     */
    JCalendar,

    /**
     * Uses <a href="https://github.com/LGoodDatePicker/LGoodDatePicker">LGoodDatePicker</a>.
     * Version: 10.3.1
     */
    LGoodDatePicker
}
