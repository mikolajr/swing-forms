/**
 * 
 */
package org.rydzewski.swing.binding;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Binds object property to document's element
 * @author Mikolaj Rydzewski
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Mapping {

    /**
     * Is the field required
     * @return
     */
    boolean required() default false;
    
    /**
     * Maximum allowed length
     * @return
     */
    int length() default -1;
    
    /**
     * Human readable name
     * @return
     */
    String label() default "";
    
    /**
     * Regular expression to verify correctness of data
     * @return
     */
    String regexp() default "";
    
    /**
     * Name of target element, if different than de-camelized field name
     * @return
     */
    String element() default "";
    
    /**
     * If set, field's value must be a numeric value with given number of decimal digits after poiny
     * @return
     */
    int scale() default -1;
    
    /**
     * Whether mapped control should be read-only
     * @return
     */
    boolean readonly() default false;
    
    /**
     * @return
     */
    MappedComponent type() default MappedComponent.TEXT;
    
    /**
     * Optional parameter for type attribute
     * @return
     */
    String typeParameter() default "";
    
    /**
     * Tooltip for field
     * @return
     */
    String tooltip() default "";
    
    /**
     * Whether this control should be disabled
     * @return
     */
    boolean disabled() default false;
    
    /**
     * Additional constraints
     * @return
     */
    Constraint[] constraint() default {};

    DateTimeSettings dateTimeSettings() default @DateTimeSettings();
}
