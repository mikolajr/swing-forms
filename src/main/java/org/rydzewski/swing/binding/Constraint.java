/**
 * 
 */
package org.rydzewski.swing.binding;

/**
 * Additional constraints applied to {@link org.rydzewski.swing.binding.Mapping} elements
 * @author Mikolaj Rydzewski
 */
public enum Constraint {
    
    /**
     * numeric value >= 0 
     */
    GREATER_THAN_ZERO,

	GREATER_OR_EQUALS_ZERO,

	LOWER_OR_EQUALS_ZERO
}
