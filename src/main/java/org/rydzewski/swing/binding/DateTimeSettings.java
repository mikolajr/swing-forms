package org.rydzewski.swing.binding;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Customizations for date and time bindings.
 *
 * @author wzupa
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DateTimeSettings {

    /**
     * Date/time picker's visual component library to use.
     */
    DateTimeComponentLibrary componentLibrary() default DateTimeComponentLibrary.JCalendar;

    /**
     * Date picker's date format.
     */
    String dateFormat() default "yyyy-MM-dd";

    /**
     * Time picker's time format.
     */
    String timeFormat() default "HH:ii";

}
