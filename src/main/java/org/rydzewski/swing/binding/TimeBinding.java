/**
 * 
 */
package org.rydzewski.swing.binding;

import org.rydzewski.swing.components.TimeComponent;

import javax.swing.*;
import java.lang.reflect.Field;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class TimeBinding extends Binding {
    
    public TimeBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public String getComponentValue() {
        return getComponent().getValue();
    }

    @Override
    public void setComponentValue(Object value) {
        if (value != null) {
            getComponent().setValue(value.toString());
        } else {
            getComponent().setValue(0, 0);
        }
    }

    @Override
    public TimeComponent getComponent() {
        return (TimeComponent) super.getComponent();
    }

}
