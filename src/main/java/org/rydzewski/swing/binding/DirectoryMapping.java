package org.rydzewski.swing.binding;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specify additional settings for directory selection mapping.
 *
 * @author wzupa
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DirectoryMapping {

	/**
	 * Path to icon file placed on button.
	 */
	String buttonIcon() default "";

	/**
	 * Message code for button text.
	 */
	String buttonText() default "";

	/**
	 * Specifies if text field with selected directory path should be read only.
	 */
	boolean fieldReadOnly() default false;

	/**
	 * {@link javax.swing.JFileChooser} bean to use.<br>
	 * If this is empty, default one will be created.
	 */
	String fileChooserBean() default "";

	/**
	 * Root directory to set in case {@link DirectoryMapping#fileChooserBean()} is empty.
	 */
	String rootDirectory() default "";
}
