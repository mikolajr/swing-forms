package org.rydzewski.swing.binding;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.Date;


/**
 *
 * @author Mikolaj Rydzewski
 */
public class LabelBinding extends Binding {

    public final static DateTimeFormatter DATE_YYYY_MM_DD = DateTimeFormat.forPattern("yyyy-MM-dd");

    public LabelBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public JLabel getComponent() {
        return (JLabel) super.getComponent();
    }

    @Override
    public String getComponentValue() {
        return getComponent().getText();
    }

    @Override
    public void setComponentValue(Object value) {
        if (value instanceof Date) {
            getComponent().setText(DATE_YYYY_MM_DD.print(((Date) value).getTime()));
        } else if (value != null) {
            getComponent().setText(value.toString());
        }
    }

}
