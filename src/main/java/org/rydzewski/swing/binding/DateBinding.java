/**
 * 
 */
package org.rydzewski.swing.binding;

import org.rydzewski.swing.components.DateComponent;

import javax.swing.JComponent;
import java.lang.reflect.Field;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class DateBinding extends Binding {

    private final DateTimeFormatter dateTimeFormatter;

    private DateComponent dateComponent;

    public DateBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(mapping.dateTimeSettings().dateFormat());
        this.dateComponent = (DateComponent) component;
        this.dateComponent.setFormatter(dateTimeFormatter);
    }

    @Override
    public String getComponentValue() {
        return dateComponent.getStringValue();
    }

    @Override
    public void setComponentValue(Object value) {
        dateComponent.setValue(value);
    }

}
