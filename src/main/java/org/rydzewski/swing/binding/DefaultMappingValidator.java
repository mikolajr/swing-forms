/**
 * 
 */
package org.rydzewski.swing.binding;

import org.apache.commons.lang.StringUtils;
import org.rydzewski.swing.binding.errors.BindingError;
import org.rydzewski.swing.binding.errors.ConstraintBindingError;
import org.rydzewski.swing.binding.errors.LengthBindingError;
import org.rydzewski.swing.binding.errors.NumericBindingError;
import org.rydzewski.swing.binding.errors.RegexpBindingError;
import org.rydzewski.swing.binding.errors.RequiredBindingError;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class to externalize validation of value with associated {@link Mapping}
 * @author Mikolaj Rydzewski
 */
public class DefaultMappingValidator implements MappingValidator {
    
    public BindingError validate(Mapping mapping, String value, Pattern pattern, Object bean, String property) {
        if (StringUtils.isNotBlank(mapping.regexp()) && pattern == null) {
            pattern = Pattern.compile(mapping.regexp());
        }
        
        if (StringUtils.isEmpty(value) && mapping.required())
            return new RequiredBindingError(mapping, bean, property);
        
        if (StringUtils.isEmpty(value) && !mapping.required())
            return null;
        
        if (mapping.length() != -1 && value.length() > mapping.length())
            return new LengthBindingError(mapping, value.length(), bean, property);

        if (pattern !=  null) {
            Matcher matcher = pattern.matcher(value);
            if (!matcher.matches())
                return new RegexpBindingError(mapping, bean, property);
        }
        
        if (mapping.scale() != -1) {
            try {
                BigDecimal number = new BigDecimal(value);
                if (number.scale() > mapping.scale())
                    return new NumericBindingError(mapping, bean, property);
            }
            catch (Exception e) {
                return new NumericBindingError(mapping, bean, property);
            }
        }
        
        if (mapping.constraint() != null) {
            for(Constraint constraint : mapping.constraint()) {
	            ConstraintValidator constraintValidator = new ConstraintValidator(constraint);
	            constraintValidator.validate(value);
                if (!constraintValidator.isValid()) {
                	return new ConstraintBindingError(mapping, bean, property, constraintValidator.getErrorCode());
                }
            }
        }
        
        return null;
    }

}
