package org.rydzewski.swing.binding;

import org.rydzewski.swing.components.DirectoryChooser;

import javax.swing.JComponent;
import java.lang.reflect.Field;

/**
 * @author wzupa
 */
public class DirectoryBinding extends Binding {

	public DirectoryBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
		super(field, mapping, component, bean, property);
	}

	@Override
	public Object getComponentValue() {
		return getComponent().getValue();
	}

	@Override
	public void setComponentValue(Object value) {
		getComponent().setValue(value != null ? value.toString() : "");
	}

	@Override
	public DirectoryChooser getComponent() {
		return (DirectoryChooser) super.getComponent();
	}
}
