/**
 * 
 */
package org.rydzewski.swing.binding;

import org.rydzewski.swing.components.JFileChooserTextField;

import javax.swing.*;
import java.lang.reflect.Field;

/**
 * File chooser binding
 * @author Mikolaj Rydzewski
 */
public class FileBinding extends Binding {

    public FileBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public String getComponentValue() {
        return getComponent().getText();
    }

    @Override
    public void setComponentValue(Object value) {
        getComponent().setText((String) value);
    }

    @Override
    public JFileChooserTextField getComponent() {
        return (JFileChooserTextField) super.getComponent();
    }

}
