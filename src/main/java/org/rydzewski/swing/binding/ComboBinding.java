package org.rydzewski.swing.binding;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.rydzewski.swing.components.ComboDataProvider;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;


/**
 *
 * @author Mikolaj Rydzewski
 */
public class ComboBinding extends Binding {
    
    private ComboDataProvider dataProvider;

    public ComboBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }
    
    public String getComponentLabel() {
        Object item = getComponent().getSelectedItem();
        if (item instanceof LabelValue) {
            return ((LabelValue) item).getLabel();
        }
        return item != null ? item.toString() : null;
    }

    @Override
    public String getComponentValue() {
        Object item = getComponent().getSelectedItem();
        if (item instanceof LabelValue) {
            return ((LabelValue) item).getValue();
        }
        return item != null ? item.toString() : null;
    }

    @Override
    public void setComponentValue(Object value) {
        if (value instanceof LabelValue) {
            getComponent().setSelectedItem(value);
        } else {
            JComboBox comboBox = getComponent();
            for(int i=0; i<comboBox.getItemCount(); i++) {
                Object item = comboBox.getItemAt(i);
                if (item instanceof LabelValue) {
                    if (((LabelValue)item).getValue().equals(value)) {
                        comboBox.setSelectedItem(item);
                        return;
                    }
                }
            }
            comboBox.setSelectedItem(value);
        }
    }

    private boolean containsValue(Object value) {
        JComboBox comboBox = getComponent();
        for(int i=0; i<comboBox.getItemCount(); i++) {
            Object item = comboBox.getItemAt(i);
            if (item instanceof LabelValue) {
                if (((LabelValue)item).getValue().equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public JComboBox getComponent() {
        return (JComboBox) super.getComponent();
    }

    public ComboDataProvider getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(ComboDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public void readFromBean() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object value = PropertyUtils.getProperty(getBean(), getProperty());
        if (getComponent().isEditable() || containsValue(value)) {
            setComponentValue(value);
        }
    }

    @Override
    public void initialize() {
        final JComboBox comboBox = getComponent();
        comboBox.removeAllItems();
        if (dataProvider != null) {
            if (dataProvider.isAllowEmptyValue()) {
                comboBox.addItem("");
            }
            List<LabelValue> values = dataProvider.getValues(getComponent());
            CollectionUtils.forAllDo(values, new Closure() {
                @Override
                public void execute(Object input) {
                    comboBox.addItem(input);
                }
            });
        } else {
            comboBox.addItem("");
        }
    }
}

