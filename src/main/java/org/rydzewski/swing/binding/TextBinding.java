package org.rydzewski.swing.binding;

import javax.swing.*;
import java.lang.reflect.Field;


/**
 *
 * @author Mikolaj Rydzewski
 */
public class TextBinding extends Binding {

    public TextBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public JTextField getComponent() {
        return (JTextField) super.getComponent();
    }

    @Override
    public String getComponentValue() {
        return getComponent().getText();
    }

    @Override
    public void setComponentValue(Object value) {
        getComponent().setText((String) value);
        getComponent().select(0, 0);
    }

}
