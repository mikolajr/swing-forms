/**
 * 
 */
package org.rydzewski.swing.binding;

import org.rydzewski.swing.binding.errors.BindingError;

import java.util.regex.Pattern;

/**
 *
 * @author Mikolaj Rydzewski
 */
public interface MappingValidator {
    
    public BindingError validate(Mapping mapping, String value, Pattern pattern, Object bean, String property);

}
