/**
 * 
 */
package org.rydzewski.swing.binding;


/**
 * Component type to be mapped by {@link org.rydzewski.swing.binding.Mapping}
 * @author Mikolaj Rydzewski
 */
public enum MappedComponent {
    
    /**
     * single line text edit field - JTextField}
     */
    TEXT,
    
    /**
     * multiple line text edit field
     */
    TEXTAREA,
    
    /**
     * read only label
     */
    LABEL,
    
    /**
     * read only label with HTML formatting - encloses text with &lt;html&gt;&lt;p&gt; ... 
     */
    HTML_LABEL,

    /**
     * drop down list - JComboBox 
     */
    COMBO,
    
    /**
     * read only drop down list
     */
    COMBO_RO,
    
    /**
     * date entry field - JSimpleDateChooser
     */
    DATE,
    
    /**
     * time entry field - TimeComponent
     */
    TIME,
    
    /**
     * password entry field - JPasswordField
     */
    PASSWORD,
    
    /**
     * file chooser field - JFileChooserTextField
     */
    FILE,
    
    /**
     * directory chooser field - JFileChooserTextField
     */
    DIRECTORY,

	/**
	 * directory chooser with customized L&F
	 */
	DIRECTORY_CUSTOMIZED,
    
    /**
     * checkbox component for boolean values - JCheckBox
     */
    CHECKBOX,
    
    /**
     * text entry field with integer values
     */
    INTEGER,
    
    /**
     * color entry field - JColorChooser 
     */
    COLOR,

    /**
     * text entry with big decimal
     */
    BIG_DECIMAL
    
}
