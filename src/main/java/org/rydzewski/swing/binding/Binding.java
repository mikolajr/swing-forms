/**
 * 
 */
package org.rydzewski.swing.binding;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.rydzewski.swing.binding.errors.BindingError;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;


/**
 * 
 * @author Mikolaj Rydzewski
 */
public abstract class Binding implements Verifyiable {

    /**
     * mapping with meta information related to field
     */
    private Mapping mapping;
    
    /**
     * field in source java bean with value bound
     */
    private Field field;
    
    /**
     * java bean object which contains this bound field
     */
    private Object bean;
    
    /**
     * property name, relative to bean, pointing to field bound. might be a nested property 
     */
    private String property;
    
    /**
     * component bound to field
     */
    protected JComponent component;
    
    /**
     * associated label (if any) 
     */
    private JLabel label;
    
    /**
     * precompiled regexp pattern for data validation (if any) 
     */
    private Pattern pattern;
    
    /**
     * original component's border - we change its border when data validation fails, so we need
     * to save original one
     */
    private Border border;
    
    /**
     * related bindings that this binding should cascade 'enabled' property changes to
     */
    private Binding[] relatedBindings;
    
    /**
     * related components that this binding should cascade 'enabled' property changes to
     */
    private Component[] relatedComponents;
    
    private static final MappingValidator mappingValidator = new DefaultMappingValidator();

    public Binding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super();
        this.field = field;
        this.mapping = mapping;
        this.component = component;
        this.bean = bean;
        this.property = property;
        if (StringUtils.isNotBlank(mapping.regexp())) {
            pattern = Pattern.compile(mapping.regexp());
        }
        border = component.getBorder();
        component.addPropertyChangeListener("enabled", enabledPropertyChangeListener);
        component.addPropertyChangeListener("visible", visiblePropertyChangeListener);
    }
    
    /**
     * Copies value from bean to component 
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     */
    public void readFromBean() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Object value = PropertyUtils.getProperty(bean, property);
        setComponentValue(value);
    }
    
    /**
     * Copies value from component to bean
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws NoSuchMethodException 
     */
    public void writeToBean() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        PropertyUtils.setProperty(bean, property, getComponentValue());
    }

    public abstract Object getComponentValue();
    
    public abstract void setComponentValue(Object value);
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bean == null) ? 0 : bean.hashCode());
        result = prime * result + ((field == null) ? 0 : field.hashCode());
        result = prime * result + ((property == null) ? 0 : property.hashCode());
        return result;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public Mapping getMapping() {
        return mapping;
    }

    public void setMapping(Mapping mapping) {
        this.mapping = mapping;
    }

    public JComponent getComponent() {
        return component;
    }

    public void setComponent(JComponent component) {
        this.component = component;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }
    
    public void initialize() {
    }

    @Override
    public BindingError verify() {
        if (!isVerificationAllowed()) {
            return null;
        }
        String value = getComponentValueForVerification();
        return mappingValidator.validate(mapping, value, pattern, bean, property);
    }
    
    @Override
    public boolean isVerificationAllowed() {
        return true;
    }

    protected String getComponentValueForVerification() {
        return (String) getComponentValue();
    }
    
    public Pattern getPattern() {
        return pattern;
    }
    
    public void setErrorState() {
        component.setBorder(BorderFactory.createLineBorder(Color.RED));
    }
    public void setNormalState() {
        component.setBorder(border);
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    public void setEnabled(boolean enabled) {
        component.setEnabled(mapping.disabled() ? false : enabled);
        if (label != null) {
            label.setEnabled(enabled);
        }
    }
    
    public void setVisible(boolean visible) {
        component.setVisible(visible);
        if (label != null) {
            label.setVisible(visible);
        }
    }

    protected void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public boolean isEmpty() {
        Object value = getComponentValue();
        return value == null || StringUtils.isBlank(value.toString());
    }

    public JLabel getLabel() {
        return label;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Binding other = (Binding) obj;
        if (bean == null) {
            if (other.bean != null) return false;
        } else if (!bean.equals(other.bean)) return false;
        if (field == null) {
            if (other.field != null) return false;
        } else if (!field.equals(other.field)) return false;
        if (property == null) {
            if (other.property != null) return false;
        } else if (!property.equals(other.property)) return false;
        return true;
    }

    public String getProperty() {
        return property;
    }
    
    /**
     * related bindings that this binding should cascade 'enabled' property changes to
     * @param relatedBindings
     */
    public void setRelatedBindings(Binding... relatedBindings) {
        this.relatedBindings = relatedBindings;
    }
    
    /**
     * related components that this binding should cascade 'enabled' property changes to
     * @param relatedComponents
     */
    public void setRelatedComponents(Component... relatedComponents) {
        this.relatedComponents = relatedComponents;
    }

    /**
     * listens for changes in enabled property - cascades events to associated bindings/components  
     */
    private PropertyChangeListener enabledPropertyChangeListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            boolean enabled = (Boolean) evt.getNewValue();
            
            if (relatedBindings != null) {
                for(Binding b : relatedBindings) {
                    b.setEnabled(enabled);
                }
            }
            
            if (relatedComponents !=  null) {
                for(Component c : relatedComponents) {
                    c.setEnabled(enabled);
                }
            }
        }
    };

    /**
     * listens for changes in visible property - cascades events to associated bindings/components  
     */
    private PropertyChangeListener visiblePropertyChangeListener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            boolean visible = (Boolean) evt.getNewValue();
            
            if (relatedBindings != null) {
                for(Binding b : relatedBindings) {
                    b.setVisible(visible);
                }
            }
            
            if (relatedComponents !=  null) {
                for(Component c : relatedComponents) {
                    c.setVisible(visible);
                }
            }
        }
    };
}
