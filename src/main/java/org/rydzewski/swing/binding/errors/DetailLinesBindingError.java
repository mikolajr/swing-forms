/**
 * 
 */
package org.rydzewski.swing.binding.errors;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class DetailLinesBindingError extends EmptyBindingError {

    public DetailLinesBindingError() {
        super(null);
    }

    @Override
    public String getMessage() {
        return getString("binding.error.details");
    }
}
