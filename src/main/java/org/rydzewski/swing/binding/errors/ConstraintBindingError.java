package org.rydzewski.swing.binding.errors;

import org.rydzewski.swing.binding.Mapping;

/**
 * @author wzupa
 */
public class ConstraintBindingError extends LocalizedBindingError {

	private String errorCode;

	public ConstraintBindingError(Mapping binding, Object bean, String property, String errorCode) {
		super(binding, bean, property);
		this.errorCode = errorCode;
	}

	@Override
	public String getMessage() {
		return super.getMessage() + getString(errorCode);
	}
}
