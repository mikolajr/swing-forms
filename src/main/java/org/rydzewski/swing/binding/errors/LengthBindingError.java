/**
 * 
 */
package org.rydzewski.swing.binding.errors;

import org.rydzewski.swing.binding.Mapping;

import java.text.MessageFormat;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public class LengthBindingError extends LocalizedBindingError {

    private final int currentLength;

    public LengthBindingError(Mapping binding, int currentLength, Object bean, String property) {
        super(binding, bean, property);
        this.currentLength = currentLength;
    }

    @Override
    public String getMessage() {
        return super.getMessage() + MessageFormat.format(getString("binding.error.length"), currentLength, getMapping().length());
    }
}
