/**
 * 
 */
package org.rydzewski.swing.binding.errors;

import org.rydzewski.swing.binding.Mapping;
import org.rydzewski.swing.binding.TextBinding;

import javax.swing.*;
import java.lang.reflect.Field;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class PasswordBinding extends TextBinding {

    public PasswordBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public String getComponentValue() {
        return new String(getComponent().getPassword());
    }

    @Override
    public JPasswordField getComponent() {
        return (JPasswordField)component;
    }

}
