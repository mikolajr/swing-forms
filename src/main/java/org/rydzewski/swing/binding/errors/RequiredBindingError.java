/**
 * 
 */
package org.rydzewski.swing.binding.errors;

import org.rydzewski.swing.binding.Mapping;

/**
 *
 * @author Mikolaj Rydzewski
 */
public class RequiredBindingError extends LocalizedBindingError {

    public RequiredBindingError(Mapping binding, Object bean, String property) {
        super(binding, bean, property);
    }

    @Override
    public String getMessage() {
        return super.getMessage() + getString("binding.error.required");
    }
}
