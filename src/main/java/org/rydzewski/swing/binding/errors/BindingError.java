/**
 * 
 */
package org.rydzewski.swing.binding.errors;

import org.rydzewski.swing.binding.Mapping;

import java.util.Collection;


/**
 *
 * @author Mikolaj Rydzewski
 */
public class BindingError {
    
    private final Mapping binding;

    public BindingError(Mapping mapping) {
        this.binding = mapping;
    }

    protected Mapping getMapping() {
        return binding;
    }
    
    public String getMessage() {
        return "Błąd w polu " + binding.label(); 
    }
    
    public static String combineMessages(Collection<BindingError> errors) {
        StringBuilder sb = new StringBuilder();
        for (BindingError be : errors) {
            sb.append(be.getMessage()).append("\n");
        }
        return sb.toString();
    }

}
