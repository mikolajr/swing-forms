package org.rydzewski.swing.binding.errors;

import org.rydzewski.swing.binding.Mapping;
import org.rydzewski.swing.i18n.LocalisationUtils;
import org.rydzewski.swing.i18n.SimpleMessageSource;

import java.text.MessageFormat;

public class LocalizedBindingError extends BindingError {

    private static SimpleMessageSource resourceBundle;
    private LocalisationUtils localisationUtils = new LocalisationUtils(resourceBundle);
    private Object bean;
    private String property;

    public LocalizedBindingError(Mapping mapping, Object bean, String property) {
        super(mapping);
        this.bean = bean;
        this.property = property;
    }

    @Override
    public String getMessage() {
        return MessageFormat.format(resourceBundle.getString("binding.error.default"), getFieldLabel());
    }

    protected String getString(String key) {
        return resourceBundle.getString(key);
    }

    private String getFieldLabel() {
        return localisationUtils.getLabelFromMapping(getMapping(), bean, property);
    }

    public static void setMessageSource(SimpleMessageSource messageSource) {
        resourceBundle = messageSource;
    }
}
