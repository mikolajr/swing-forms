/**
 * 
 */
package org.rydzewski.swing.binding.errors;

/**
 *
 * @author Mikolaj Rydzewski
 */
public abstract class EmptyBindingError extends LocalizedBindingError {

    private final String message;

    public EmptyBindingError(String message) {
        super(null, null, null);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
