/**
 *
 */
package org.rydzewski.swing.binding;

import com.jgoodies.forms.builder.PanelBuilder;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.rydzewski.swing.LittlePropertyUtils;
import org.rydzewski.swing.SimplePanelBuilder;
import org.rydzewski.swing.binding.errors.BindingError;
import org.rydzewski.swing.binding.errors.PasswordBinding;
import org.rydzewski.swing.components.ComboDataProvider;
import org.rydzewski.swing.components.DirectoryChooser;
import org.rydzewski.swing.components.JColorChooserTextField;
import org.rydzewski.swing.components.JFileChooserTextField;
import org.rydzewski.swing.components.JFileChooserTextField.Mode;
import org.rydzewski.swing.components.JSimpleDateChooser;
import org.rydzewski.swing.components.LGoodDatePicker;
import org.rydzewski.swing.components.LengthVerifier;
import org.rydzewski.swing.components.TimeComponent;
import org.rydzewski.swing.i18n.LocalisationUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import javax.swing.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Field binding utility
 *
 * @author Mikolaj Rydzewski
 */
public class FieldBinder {

	/**
	 * spring context for loading {@link org.rydzewski.swing.components.ComboDataProvider} beans
	 */
	private ApplicationContext applicationContext;

	private ResourceBundle resourceBundle;
	private LocalisationUtils localisationUtils;

	/**
	 * Collection of configured bindings
	 */
	private Set<Binding> bindings = new LinkedHashSet<Binding>();

	public FieldBinder(ApplicationContext applicationContext, ResourceBundle resourceBundle) {
		this.applicationContext = applicationContext;
		this.resourceBundle = resourceBundle;
		this.localisationUtils = new LocalisationUtils(resourceBundle);
	}

	/**
	 * Removes all bindings
	 */
	public void clear() {
		bindings.clear();
	}

	/**
	 * Adds specified binding to the list
	 *
	 * @param binding
	 */
	public void addBinding(Binding binding) {
		bindings.add(binding);
	}

	/**
	 * Iterates over all bindings and copies value from bean's property to component
	 */
	public void readDataFromBean() {
		for (Binding binding : bindings) {
			try {
				binding.readFromBean();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Iterates over all bindings and copies value from component to bean's property
	 */
	public void writeDataToBean() {
		for (Binding binding : bindings) {
			try {
				binding.writeToBean();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Initializes every binding.
	 */
	public void initialize() {
		for (Binding binding : bindings) {
			binding.initialize();
		}
	}

	/**
	 * Verifies every binding
	 *
	 * @return list with validation errors
	 */
	public List<BindingError> verifyFields() {
		List<BindingError> errors = new ArrayList<BindingError>();

		for (Binding binding : bindings) {
			if (!binding.getComponent().isEnabled() || !binding.getComponent().isVisible())
				continue;

			try {
				BindingError error = binding.verify();
				if (error != null) {
					errors.add(error);
					binding.setErrorState();
				} else {
					binding.setNormalState();
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		return errors;
	}

	/**
	 * Binds bean's property to generated component. Does not adds component to any GUI.
	 *
	 * @param bean
	 * @param property
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public Binding bindField(Object bean, String property) throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return bindField(bean, property, null, null);
	}

	public Binding bindField(Object bean, String property, Action action) throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return bindField(bean, property, null, action);
	}

	/**
	 * Binds bean's property to generated component. Does not adds component to any GUI.
	 *
	 * @param bean
	 * @param property
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public Binding bindField(Object bean, String property, String label, Action action) throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Field field = findNestedField(bean, property);
		Mapping mapping = field.getAnnotation(Mapping.class);

		if (mapping == null) {
			return null;
		}

		if (label == null) {
			label = localisationUtils.getLabelFromMapping(mapping, bean, property);
		}

		JLabel jlabel = new JLabel(label);
		JComponent component = null;
		Binding binding = null;

		switch (mapping.type()) {
			case LABEL:
				component = new JLabel();
				binding = new LabelBinding(field, mapping, component, bean, property);
				break;
			case HTML_LABEL:
				component = new JLabel();
				binding = new HtmlLabelBinding(field, mapping, component, bean, property);
				break;
			case TEXTAREA:
				component = new JTextArea();
				((JTextArea) component).setEditable(!mapping.readonly());
				binding = new TextAreaBinding(field, mapping, component, bean, property);
				break;
			case TEXT:
				component = new JTextField();
				((JTextField) component).setEditable(!mapping.readonly());
				binding = new TextBinding(field, mapping, component, bean, property);
				break;
			case INTEGER:
				component = new JTextField();
				((JTextField) component).setEditable(!mapping.readonly());
				binding = new IntegerBinding(field, mapping, component, bean, property);
				break;
			case BIG_DECIMAL:
				component = new JTextField();
				((JTextField) component).setEditable(!mapping.readonly());
				binding = new BigDecimalBinding(field, mapping, component, bean, property);
				break;
			case PASSWORD:
				component = new JPasswordField();
				((JPasswordField) component).setEditable(!mapping.readonly());
				binding = new PasswordBinding(field, mapping, component, bean, property);
				break;
			case COMBO:
				component = new JComboBox();
				((JComboBox) component).setEditable(!mapping.readonly());
				binding = new ComboBinding(field, mapping, component, bean, property);
				if (!mapping.typeParameter().isEmpty()) {
					addDataProvider((ComboBinding) binding, mapping.typeParameter());
				}
				break;
			case COMBO_RO:
				component = new JComboBox();
				((JComboBox) component).setEditable(false);
				binding = new ComboBinding(field, mapping, component, bean, property);
				if (!mapping.typeParameter().isEmpty()) {
					addDataProvider((ComboBinding) binding, mapping.typeParameter());
				}
				break;
			case DATE:
                DateTimeSettings dateTimeSettings = mapping.dateTimeSettings();
                switch (dateTimeSettings.componentLibrary()) {
                    case LGoodDatePicker:
                        component = new LGoodDatePicker();
                        break;

                    case JCalendar:
                    default:
                        component = new JSimpleDateChooser(dateTimeSettings.dateFormat());
                        break;
                }
				binding = new DateBinding(field, mapping, component, bean, property);
				break;
			case TIME:
				component = new TimeComponent(resourceBundle);
				binding = new TimeBinding(field, mapping, component, bean, property);
				break;
			case FILE:
				component = new JFileChooserTextField(Mode.OPEN, null, null, null, JFileChooser.FILES_ONLY);
				binding = new FileBinding(field, mapping, component, bean, property);
				break;
			case COLOR:
				component = new JColorChooserTextField();
				binding = new ColorBinding(field, mapping, component, bean, property);
				break;
			case DIRECTORY:
				component = new JFileChooserTextField(Mode.SAVE, null, null, null, JFileChooser.DIRECTORIES_ONLY);
				binding = new FileBinding(field, mapping, component, bean, property);
				break;
			case DIRECTORY_CUSTOMIZED:
				DirectoryMapping directoryMapping = field.getAnnotation(DirectoryMapping.class);
				if (directoryMapping == null) {
					throw new IllegalStateException("Missing @DirectoryMapping annotation data!");
				}

				String buttonLabel = !directoryMapping.buttonText().isEmpty() ? resourceBundle.getString(directoryMapping.buttonText()) : "...";

				JFileChooser jFileChooser = null;
				if (!directoryMapping.fileChooserBean().isEmpty()) {
					Object fileChooserBean = null;
					try {
						fileChooserBean = applicationContext.getBean(directoryMapping.fileChooserBean());
						if (fileChooserBean != null && fileChooserBean instanceof JFileChooser) {
							jFileChooser = (JFileChooser) fileChooserBean;
						}
					} catch (BeansException e) {
						// ignored
					}
				}

				component = new DirectoryChooser(buttonLabel, directoryMapping.buttonIcon(), directoryMapping.fieldReadOnly(),
						jFileChooser, directoryMapping.rootDirectory());
				binding = new DirectoryBinding(field, mapping, component, bean, property);
				break;
			case CHECKBOX:
				component = new JCheckBox(label);
				if (action != null) {
					((JCheckBox) component).setAction(action);
				}
				jlabel = null;
				binding = new CheckboxBinding(field, mapping, component, bean, property);
				break;
			default:
				throw new IllegalStateException("Unknown mapping type " + mapping.type());
		}

		if (mapping.length() != -1) {
			component.setInputVerifier(new LengthVerifier(mapping.length()));
		}
		binding.setLabel(jlabel);
		String tooltip = localisationUtils.getTooltipFromMapping(mapping, bean, property);
		if (tooltip != null) {
			component.setToolTipText(tooltip);
			jlabel.setToolTipText(tooltip);
		}
		if (mapping.disabled()) {
			binding.setEnabled(true);
		}
		addBinding(binding);

		return binding;
	}

	/**
	 * Returns Field object referring to nested field specified by property
	 *
	 * @param bean
	 * @param property
	 * @return
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	private Field findNestedField(Object bean, String property) throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return LittlePropertyUtils.getTargetDeclaredField(bean, property);
	}

	/**
	 * Creates binding for given bean's property. Adds created component and appropriate label to {@link PanelBuilder}
	 *
	 * @param builder  form panel builder
	 * @param bean     bean with property to be bound
	 * @param property property name
	 * @param label    label for component
	 * @param colspan
	 * @return created binding
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public Binding bindField(SimplePanelBuilder builder, Object bean, String property, String label, int colspan)
			throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Binding binding = bindField(bean, property, label, null);
		if (binding.getLabel() != null) {
			builder.addAndMove(binding.getLabel());
		}
		builder.addAndMove(binding.getComponent(), colspan);

		return binding;
	}

	/**
	 * Creates binding for given bean's property. Adds created component without any label to {@link PanelBuilder}
	 *
	 * @param builder
	 * @param bean
	 * @param property
	 * @param colspan
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public Binding bindFieldNoLabel(SimplePanelBuilder builder, Object bean, String property, int colspan)
			throws SecurityException, NoSuchFieldException, IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		Binding binding = bindField(bean, property);
		builder.addAndMove(binding.getComponent(), colspan);
		return binding;
	}

	private void addDataProvider(ComboBinding binding, String parameter) {
		Object bean = applicationContext.getBean(parameter);
		if (bean instanceof ComboDataProvider) {
			binding.setDataProvider((ComboDataProvider) bean);
		}
	}

	/**
	 * Creates binding for given bean's property. Adds created component and appropriate label to {@link PanelBuilder}
	 *
	 * @param builder  form panel builder
	 * @param bean     bean with property to be bound
	 * @param property property name
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public Binding bindField(SimplePanelBuilder builder, Object bean, String property, int colspan) throws SecurityException,
			NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return bindField(builder, bean, property, null, colspan);
	}

	/**
	 * Creates binding for given bean's property. Adds created component and appropriate label to {@link PanelBuilder}
	 *
	 * @param builder  form panel builder
	 * @param bean     bean with property to be bound
	 * @param property property name
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 * @throws IllegalAccessException
	 */
	public Binding bindField(SimplePanelBuilder builder, Object bean, String property) throws SecurityException,
			NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		return bindField(builder, bean, property, null, 1);
	}

	/**
	 * Iterates over all bindings with given closure.
	 *
	 * @param closure
	 */
	public void forAllDo(Closure closure) {
		CollectionUtils.forAllDo(bindings, closure);
	}
}
