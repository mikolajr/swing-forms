/**
 * 
 */
package org.rydzewski.swing.binding;

import org.rydzewski.swing.binding.errors.BindingError;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public interface Verifyiable {
    
    /**
     * Verifies current value
     * @return
     */
    public BindingError verify();
    
    /**
     * Whether object allows to verify itself or not
     * @return
     */
    public boolean isVerificationAllowed();

}
