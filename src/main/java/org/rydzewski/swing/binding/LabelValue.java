/**
 * 
 */
package org.rydzewski.swing.binding;

import javax.swing.*;

/**
 * Value object for use with {@link ComboBinding}.
 * Provides following properties:
 * <ul>
 * <li>label - to be presented in component
 * <li>value - string value, ID, to be saved to database
 * <li>object - any object whose label/value this bean refers to
 * </ul>
 * {@link #toString()} method returns {@link #label} - for easy use as {@link JComboBox}
 * element.
 * @author Mikolaj Rydzewski
 */
public class LabelValue implements Comparable<LabelValue> {
    
    private String label;
    private String value;
    private Object object;
    
    public LabelValue() {
    }
    public LabelValue(String label, String value) {
        super();
        this.label = label;
        this.value = value;
        this.object = value;
    }
    public LabelValue(String label, String value, Object object) {
        super();
        this.label = label;
        this.value = value;
        this.object = object;
    }
    @Override
    public String toString() {
        return label;
    }
    public String getLabel() {
        return label;
    }
    public String getValue() {
        return value;
    }
    public Object getObject() {
        return object;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((object == null) ? 0 : object.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        LabelValue other = (LabelValue) obj;
        if (object == null) {
            if (other.object != null) return false;
        } else if (!object.equals(other.object)) return false;
        return true;
    }
    @Override
    public int compareTo(LabelValue o) {
        return label.compareTo(o.label);
    }

}
