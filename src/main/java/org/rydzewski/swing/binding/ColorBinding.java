/**
 * 
 */
package org.rydzewski.swing.binding;

import org.rydzewski.swing.components.JColorChooserTextField;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.Field;

/**
 * Color chooser binding
 * @author Mikolaj Rydzewski
 */
public class ColorBinding extends Binding {

    public ColorBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public Object getComponentValue() {
        return getComponent().getColor();
    }

    @Override
    public void setComponentValue(Object value) {
        getComponent().setColor((Color) value);
    }

    @Override
    public JColorChooserTextField getComponent() {
        return (JColorChooserTextField) super.getComponent();
    }

    @Override
    public boolean isVerificationAllowed() {
        return false;
    }

    @Override
    public void setLabel(JLabel label) {
        super.setLabel(label);
        getComponent().setTitle(label.getText());
    }

}
