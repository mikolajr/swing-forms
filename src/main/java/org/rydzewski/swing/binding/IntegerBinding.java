/**
 * 
 */
package org.rydzewski.swing.binding;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.regex.Pattern;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public class IntegerBinding extends Binding {

    public static final String NUMERIC_PATTERN = "[-]{0,1}[0-9]*";

    public IntegerBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
        if (getPattern() == null) {
            setPattern(Pattern.compile(NUMERIC_PATTERN));
        }
    }

    @Override
    public Integer getComponentValue() {
        try {
            return Integer.valueOf(getComponent().getText());
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public void setComponentValue(Object value) {
        getComponent().setText(value != null ? value.toString() : null);
    }

    @Override
    public JTextField getComponent() {
        return (JTextField) super.getComponent();
    }

    @Override
    protected String getComponentValueForVerification() {
        return getComponent().getText();
    }

}
