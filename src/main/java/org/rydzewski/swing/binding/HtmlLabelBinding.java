package org.rydzewski.swing.binding;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.Date;

/**
 * 
 * @author Mikolaj Rydzewski
 */
public class HtmlLabelBinding extends LabelBinding {

    public final static DateTimeFormatter DATE_YYYY_MM_DD = DateTimeFormat.forPattern("yyyy-MM-dd");

    public HtmlLabelBinding(Field field, Mapping mapping, JComponent component, Object bean, String property) {
        super(field, mapping, component, bean, property);
    }

    @Override
    public void setComponentValue(Object value) {
        String stringValue = null;
        if (value instanceof Date) {
            stringValue = DATE_YYYY_MM_DD.print(((Date) value).getTime());
        } else if (value != null) {
            stringValue = value.toString();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("<html><p>").append(stringValue).append("</p></html>");
        super.setComponentValue(sb.toString());
    }
}
